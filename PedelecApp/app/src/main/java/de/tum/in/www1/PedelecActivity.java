package de.tum.in.www1;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import de.tum.in.www1.model.Reservation;

public class PedelecActivity extends AppCompatActivity {

    TextView pedelecName;
    DatePicker datePicker;
    TimePicker timePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedelec);
        pedelecName = (TextView)findViewById(R.id.pedelecName);
        datePicker = (DatePicker)findViewById(R.id.datePicker);
        timePicker = (TimePicker)findViewById(R.id.timePicker);
    }

    public void onReserveTapped(View button) {
        Log.d("PedelecApp","Reserve tapped");

        final String bikeName= (String) pedelecName.getText();
        final String dateString = datePicker.getDayOfMonth() + "." + datePicker.getMonth() + "." + datePicker.getYear();
        final String timeString = timePicker.getCurrentHour() + ":" + timePicker.getCurrentMinute();

        final AlertDialog.Builder confirmationDialog = new AlertDialog.Builder(this);
        final String message = "Please confirm your reservation of " + bikeName + " at " + dateString + " " + timeString;
        confirmationDialog.setMessage(message);
        confirmationDialog.setTitle("Confirm");
        confirmationDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Reservation Reservation = new Reservation();
                Reservation.setBike(bikeName);
                Reservation.setStartTime(dateString);
                Reservation.setStartDate(timeString);
                Reservation.save();
                Log.d("PedelecApp","Reservation confirmed");
        }
        }).setCancelable(true);
        confirmationDialog.setNegativeButton("Cancel", null);
        confirmationDialog.create().show();
    }
}
